#! /usr/bin/env python3

import random

# Συνάρτηση που τσεκάρει την είσοδο από το χρήστη και
# δημιουργεί λεξικό με το αν είναι αποδεκτή και μήνυμα
# σφάλματος. Για αρχή τσεκάρει αν η είσοδος είναι ακέραιος.
# Στην περίπτωση που είναι ελέγχει αν βρίσκεται εντός του
# αποδεκτού διαστήματος.
def isValidInput(i):
    try:
        val = int(i)
    except ValueError:
        return {'Flag':False, 'Error': 'Το στοιχείο που δώσατε δεν είναι ακέραιος.'}
    if ((val < 1) or (val > 100)) :
        return {'Flag':False, 'Error': 'Παρακαλώ δώστε αριθμό στο εύρος [1-100]'}
    else:
        return {'Flag':True, 'Error': 'No Error!'}

# Συνάρτηση που αρχικοποιεί τις συνθήκες του παιχνιδιού.
# Κατάλληλη για την επανεκκίνησή του.
def gameInit() :
    global flag, correct, tries, score
    flag = True
    score = 0
    tries = 0
    print('ΕΝΑΡΞΗ ΠΑΙΧΝΙΔΙΟΥ')
    print('\nΜΑΝΤΕΨΕ ΤΟΝ ΑΡΙΘΜΟ')
    print('Για έξοδο οποιαδήποτε στιγμή πάτησε [ENTER]')
    correct = random.randint(1,100)
    print('CORRECT: ',correct)

# Λίστες με τις αποδεκτές απαντήσεις του χρήστη για την
# επανέναρξη του παιχνιδιού.
replayYes = ['ΝΑΙ','ναι','Ναι','NAI','nai','Nai','Yes','yes','YES','Y','y']
replayNo = ['ΟΧΙ','όχι','οχι','Όχι','Οχι','OXI','oxi','Oxi','NO','No','no','N','n']

# Αρχή του παιχνιδιού με κλήση της αντίστοιχης συνάρτησης.
gameInit()

# Η κύρια επανάληψη του παιχνιδιού. Χρησιμοποιείται ώστε ο
# χρήστης να βάζει συνέχεια αριθμούς μέχρι να βρει τη σωστή
# απάντηση ή να πατήσει ENTER ώστε να τερματίσει το παιχνίδι.
while flag == True :

    uinput = input('\nΔώσε έναν αριθμό 1-100: ')
    # Τερματισμός του παιχνιδιού αν ο χρήστης πατήσει ENTER.
    if uinput == '' : break

    # Έλεγχος απάντησης του χρήστη.
    # Αν δεν είναι αποδεκτή τύπωσε το σφάλμα.
    validCheck = isValidInput(uinput)
    if validCheck['Flag'] == False :
        print(validCheck['Error'])

    else :
        # Μετατροπή της απάντησης σε integer από string
        # και υπολογισμός του score και των προσπαθειών.
        answer = int(uinput)
        if answer > correct :
            tries += 1
            print('Ο αριθμός είναι μικρότερος.')
        elif answer < correct :
            tries += 1
            print('Ο αριθμός είναι μεγαλύτερος.')
        else :
            score = 10 - tries
            if score < 0 : score = 0
            print('Το βρήκατε μετά από ', str(tries),' προσπάθειες και κερδίσατε ', str(score) ,' πόντους.')
            replay = input('Θέλετε να ξαναπαίξετε (ΝΑΙ/ΟΧΙ)? ')

            # Η επανάληψη τρέχει μέχρι ο χρήστης να δώσει μία
            # απάντηση απο τις αποδεκτές (απαντήσεις στη λίστα).
            while True :
                if replay in replayYes :
                    # Αρχικοποίηση του παιχνιδιού
                    gameInit()
                    # Έξοδος από τη λούπα για την επανέναρξη.
                    break
                elif replay in replayNo:
                    # Ορισμός της flag σε False ώστε να μην ξανατρέξει
                    # η κύρια λούπα του παιχνιδιού.
                    flag = False
                    # Έξοδος από τη λούπα για την επανέναρξη.
                    break
                else :
                    print('Παρακαλώ εισάγετε [ΝΑΙ/ΟΧΙ]')
                    replay = input('Θέλετε να ξαναπαίξετε (ΝΑΙ/ΟΧΙ)? ')

# Τέλος παιχνιδιού
print('\nΤΕΛΟΣ ΠΑΙΧΝΙΔΙΟΥ')
