#! /usr/bin/env python3
import random
import os


# Λίστες με τις αποδεκτές απαντήσεις του χρήστη για την
# επανέναρξη του παιχνιδιού.
replayYes = ['ΝΑΙ','ναι','Ναι','NAI','nai','Nai','Yes','yes','YES','Y','y']
replayNo = ['ΟΧΙ','όχι','οχι','Όχι','Οχι','OXI','oxi','Oxi','NO','No','no','N','n']


# Συνάρτηση που τσεκάρει την είσοδο από το χρήστη και
# δημιουργεί λεξικό με το αν είναι αποδεκτή και μήνυμα
# σφάλματος. Για αρχή τσεκάρει αν η είσοδος είναι ακέραιος.
# Στην περίπτωση που είναι ελέγχει αν βρίσκεται εντός του
# αποδεκτού διαστήματος.
def isValidInput(i):
    try:
        val = int(i)
    except ValueError:
        return {'Flag':False, 'Error': 'Το στοιχείο που δώσατε δεν είναι ακέραιος.'}
    if ((val < 1) or (val > 100)) :
        return {'Flag':False, 'Error': 'Παρακαλώ δώστε αριθμό στο εύρος [1-100]'}
    else:
        return {'Flag':True, 'Error': 'No Error!'}

# Η κύρια συνάρτηση του παιχνιδιού, η οποία καλεί
# αναδρομικά (recursive) τον εαυτό της σε περίπτωση που
# ο χρήστης θέλει να ξαναπαίξει.
def main():
    os.system('clear')
    # Αρχικοποίηση μεταβλητών.
    score = 0
    tries = 0
    uinput = 'run'
    print('EKSW uinput ', uinput)

    correct = random.randint(1,100)
    print('Correct: ', correct)

    # Οδηγίες παιχνιδιού
    print('\nΜΑΝΤΕΨΕ ΤΟΝ ΑΡΙΘΜΟ')
    print('Για έξοδο οποιαδήποτε στιγμή πάτησε [ENTER]')
    # Η επανάληψη συνεχίζεται μέχρι ο χρήστης να πατήσει
    # [ENTER] ή να βρει ο χρήστης την απάντηση.
    while not uinput == '': # Επανάληψη Νο.1
        uinput = input('\nΔώσε έναν αριθμό 1-100: ')
        print('MESA uinpu ',uinput)
        if uinput == '' : print('\nΤΕΛΟΣ ΠΑΙΧΝΙΔΙΟΥ'); break
        validCheck = isValidInput(uinput)
        # Αν δεν είναι αποδεκτή η απάντηση τύπωσε το σφάλμα.
        if validCheck['Flag'] == False :
            print(validCheck['Error'])
        else:
            answer = int(uinput)
            if answer == correct :
                # Υπολογισμός βαθμολογίας
                score = 10 - tries
                if score < 0 : score = 0
                print('Το βρήκατε μετά από ', str(tries),' προσπάθειες και κερδίσατε ', str(score) ,' πόντους.')
                replay = input('Θέλετε να ξαναπαίξετε (ΝΑΙ/ΟΧΙ)? ')

                # Επανάληψη για την επανεκκίνηση του παιχνιδιού.
                while True: # Επανάληψη Νο.2
                    if replay in replayYes:
                        main()
                    elif replay in replayNo:
                        print('\nΤΕΛΟΣ ΠΑΙΧΝΙΔΙΟΥ')
                        # Hard coded τιμή για να μην ξανατρέξει η επανάληψη Νο.1
                        uinput = ''
                        # Έξοδος από την επανάληψη Νο.2
                        break
                    else:
                        # Σε περίπτωση που ο χρήστης δε δώσει καμία αποδεκτή
                        # απάντηση, από τις λίστες replayYes και replayNo
                        print('Παρακαλώ εισάγετε [ΝΑΙ/ΟΧΙ]')
                        replay = input('Θέλετε να ξαναπαίξετε (ΝΑΙ/ΟΧΙ)? ')
            else :
                # Υπολογισμός προσπαθειών
                tries += 1
                if answer < correct :
                    print('Ο κρυμμένος αριθμός είναι μεγαλύτερος')
                else :
                    print('Ο κρυμμένος αριθμός είναι μικρότερος')

# Κυρίως πρόγραμμα
print('ΕΝΑΡΞΗ ΠΑΙΧΝΙΔΙΟΥ')

main()
print('ΤΕΛΟΣ ΠΑΙΧΝΙΔΙΟΥ')
